import os
import sys
import time
from multiprocessing import Process

import numpy
import pyautogui
import pytesseract
from PIL import Image


def image_path(filename):
    print(os.getcwd())
    script_dir = "./images"
    return os.path.join(script_dir, filename)


def get_meter(filename):
    image = Image.open(image_path(filename))
    number = pytesseract.image_to_string(image)
    return number


def press_key(key, interval=0.0):
    pyautogui.press(key, interval=interval)


def play_random():
    for i in range(0, 5):
        press_key("q")
        press_key("q")
        press_key("o")
        press_key("p")


def start_game(filename):
    location = pyautogui.locateOnScreen(image_path(filename))
    if location is not None:
        print(location)
        x, y = pyautogui.center(location)
        pyautogui.click(x, y)
    else:
        raise Exception('Could not find game on screen. Is the game visible?')
        # pyautogui.click(x=600, y=600, clicks=2)


def show_usage():
    print("Usage: python test.py [command]")
    print("Command list: recognition, play")


def get_game_region():
    SCREEN_SIZE = [768, 480]
    game_region = pyautogui.locateOnScreen(image_path("game_screen2.png"))
    game_region = (game_region[0], game_region[1], SCREEN_SIZE[0], SCREEN_SIZE[1])
    print("game screen information: ", game_region)
    return game_region


def capture_game(game_region):
    cap_number = 10
    interval = 100 / 1000  # 100ms
    for i in range(cap_number):
        """capture game screen"""
        # image = pyautogui.screenshot(region=game_region)
        # image.save(os.path.join("./", 'cap_{0:03d}.png'.format(i)))
        # time.sleep(interval)
        # print("sleep now")

        """capture meter"""
        offset = (274, 26)
        meter_size = (190, 30)
        meter_region = (game_region[0] + offset[0], game_region[1] + offset[1], meter_size[0], meter_size[1])
        image = pyautogui.screenshot(region=meter_region)
        image.save(os.path.join("./images/", 'meter_{0:03d}.png'.format(i)))
        time.sleep(interval)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        show_usage()
        exit(-1)

    if sys.argv[1] == "recognition":
        print(get_meter("meter_sample.png"))
    elif sys.argv[1] == "play":
        game_region = get_game_region()
        start_game("start3.png")
        capture_process = Process(target=capture_game, args=(game_region,))
        capture_process.start()
        play_random()
        raise NotImplementedError
    elif sys.argv[1] == "else":
        ACTIONS = [[key , j , 1 -j] for key in ["q", "w", "o", "p", "none"] for j in range(2)]
        print(ACTIONS)
    else:
        show_usage()
