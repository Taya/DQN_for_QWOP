import argparse
import random
import sys
import threading
import time

import chainer
import numpy
import pyautogui
from PIL import Image
from PyQt4.QtGui import QPixmap, QApplication
# from PyQt5.QtGui import QPixmap, QGuiApplication
from chainer import cuda

from game_controller import Controller
from net import Q

INTERVAL = 100 / 1000.0
GAMMA = 0.99
LATENT_SIZE = 256
batch_size = 64
pyautogui.PAUSE = 0

parser = argparse.ArgumentParser(description='Deep Q-learning Network for game using mouse')
parser.add_argument('--gpu', '-g', default=-1, type=int,
                    help='GPU ID (negative value indicates CPU)')
parser.add_argument('--input', '-i', default=None, type=str,
                    help='input model file path without extension')
parser.add_argument('--output', '-o', required=True, type=str,
                    help='output model file path without extension')
parser.add_argument('--INTERVAL', default=100, type=int,
                    help='INTERVAL of capturing (ms)')
parser.add_argument('--random', '-r', default=0.2, type=float,
                    help='randomness of play')
parser.add_argument('--pool_size', default=50000, type=int,
                    help='number of frames of memory pool size')
parser.add_argument('--random_reduction', default=0.000002, type=float,
                    help='reduction rate of randomness')
parser.add_argument('--min_random', default=0.1, type=float,
                    help='minimum randomness of play')
parser.add_argument('--train_term', default=4, type=int,
                    help='training term size')
parser.add_argument('--train_term_increase', default=0.00002, type=float,
                    help='increase rate of training term size')
parser.add_argument('--max_train_term', default=32, type=int,
                    help='maximum training term size')
parser.add_argument('--double_dqn', action='store_true',
                    help='use Double DQN algorithm')
parser.add_argument('--update_target_interval', default=2000, type=int,
                    help='INTERVAL to update target Q function of Double DQN')
parser.add_argument('--only_result', action='store_true',
                    help='use only reward to evaluate')
args = parser.parse_args()

only_result = args.only_result
use_double_dqn = args.double_dqn
update_target_interval = args.update_target_interval
game = Controller()
game.load_images('images')
if game.detect_position() is None:
    print("Error: cannot detect game screen position.")
    exit()
left, top, w, h = game.region()
train_width = w / 4
train_height = h / 4
random.seed()

print("configure GPU (or CPU)")
gpu_device = None
xp = numpy
q = Q(width=train_width, height=train_height, latent_size=LATENT_SIZE, action_size=game.action_size())
target_q = None
if args.gpu >= 0:
    cuda.check_cuda_available()
    cuda.get_device_from_id(args.gpu).use()
    xp = cuda.cupy
    q.to_gpu()

print("pool configure")
POOL_SIZE = args.pool_size
state_pool = numpy.zeros((POOL_SIZE, 3, train_height, train_width), dtype=numpy.float32)
action_pool = numpy.zeros((POOL_SIZE,), dtype=numpy.int32)
reward_pool = numpy.zeros((POOL_SIZE,), dtype=numpy.float32)
terminal_pool = numpy.zeros((POOL_SIZE,), dtype=numpy.float32)

print("allocate memory")
state_pool[...] = 0
action_pool[...] = 0
reward_pool[...] = 0
terminal_pool[...] = 0

if only_result:
    terminal_pool[-1] = 1
frame = 0
average_reward = 0

print("chainer setting")
optimizer = chainer.optimizers.AdaDelta(rho=0.95, eps=1e-06)
optimizer.setup(q)
optimizer.add_hook(chainer.optimizer.GradientClipping(0.1))
if args.input is not None:
    chainer.serializers.load_hdf5('{}.model'.format(args.input), q)
    chainer.serializers.load_hdf5('{}.state'.format(args.input), optimizer)

random_probability = args.random
random_reduction_rate = 1 - args.random_reduction
min_random_probability = min(random_probability, args.min_random)

print("QApplication setting")
app = QApplication(sys.argv)
window_id = app.desktop().winId()

def train():
    max_term_size = args.max_train_term
    current_term_size = args.train_term
    term_increase_rate = 1 + args.train_term_increase
    last_clock = time.clock()
    update_target_iteration = 0
    if use_double_dqn:
        target_q = q.copy()
        target_q.reset_state()
    while True:
        term_size = int(current_term_size)
        if frame < batch_size * term_size:
            continue
        batch_index = numpy.random.permutation(min(frame - term_size, POOL_SIZE))[:batch_size]
        train_image = xp.asarray(state_pool[batch_index])
        y = q(train_image)
        if use_double_dqn and update_target_iteration >= update_target_interval:
            target_q = q.copy()
            target_q.reset_state()
            with chainer.using_config('enable_backprop', False):
                target_q(xp.asarray(state_pool[batch_index]))
            update_iteration = 0
        for term in range(term_size):
            next_batch_index = (batch_index + 1) % POOL_SIZE
            train_image = xp.asarray(state_pool[next_batch_index])
            score = q(train_image)
            if only_result:
                t = xp.asarray(reward_pool[batch_index])
            else:
                if use_double_dqn:
                    eval_image = xp.asarray(state_pool[next_batch_index])
                    with chainer.using_config('enable_backprop', False):
                        target_score = target_q(eval_image)
                    best_action = cuda.to_cpu(xp.argmax(score.data, axis=1))
                    best_q = cuda.to_cpu(target_score.data)[range(batch_size), best_action]
                else:
                    best_q = cuda.to_cpu(xp.max(score.data, axis=1))
                t = xp.asarray(reward_pool[batch_index] + (1 - terminal_pool[batch_index]) * GAMMA * best_q)
            action_index = xp.asarray(action_pool[batch_index])
            loss = chainer.functions.mean_squared_error(chainer.functions.select_item(y, action_index), t)
            y = score
            q.cleargrads()
            loss.backward()
            loss.unchain_backward()
            optimizer.update()
            batch_index = next_batch_index
            print("loss", float(cuda.to_cpu(loss.data)))
            clock = time.clock()
            print("train", clock - last_clock)
            last_clock = clock
            if use_double_dqn:
                update_target_iteration += 1
        current_term_size = min(current_term_size * term_increase_rate, max_term_size)
        print("current_term_size ", current_term_size)


if __name__ == '__main__':
    try:
        # Start DQN thread
        # train_thread = threading.Thread(target=train, name="train_thread")
        # train_thread.start()

        next_clock = time.clock() + INTERVAL
        save_iter = 10000
        save_count = 0
        action = None
        action_q = q.copy()
        action_q.reset_state()

        print("Before While True")
        while True:
            if action is not None:
                print(action)
                game.play(action)

            # capture screeen
            pixmap = QPixmap.grabWindow(window_id, left, top, w, h)
            image = pixmap.toImage()
            bits = image.bits()
            bits.setsize(image.byteCount())
            screen = Image.fromarray(numpy.array(bits).reshape((h, w, 4))[:, :, 2::-1])
            reward, terminal = game.process(screen)
            print(reward, terminal)
            if reward is not None:
                train_image = xp.asarray(screen.resize((train_width, train_height))).astype(numpy.float32).transpose(
                    (2, 0, 1))
                train_image = xp.expand_dims(train_image, 0) / 127.5 - 1
                with chainer.using_config('train', False):
                    with chainer.using_config('enable_backprop', False):
                        score = action_q(train_image)
                best = int(numpy.argmax(score.data))
                action = game.randomize_action(best, random_probability)
                print(action, float(score.data[0][action]), best, float(score.data[0][best]), reward)
                index = frame % POOL_SIZE
                state_pool[index] = cuda.to_cpu(train_image)
                action_pool[index] = action
                reward_pool[index - 1] = reward
                average_reward = average_reward * 0.9999 + reward * 0.0001
                print("average reward: ", average_reward)
                if terminal:
                    terminal_pool[index - 1] = 1
                    if only_result:
                        i = index - 2
                        r = reward
                        while terminal_pool[i] == 0:
                            r = reward_pool[i] + GAMMA * r
                            reward_pool[i] = r
                            i -= 1
                    action_q = q.copy()
                    action_q.reset_state()
                else:
                    terminal_pool[index - 1] = 0
                frame += 1
                save_iter -= 1
                random_probability *= random_reduction_rate
                if random_probability < min_random_probability:
                    random_probability = min_random_probability
            else:
                action = None
                if save_iter <= 0:
                    print('save: ', save_count)
                    chainer.serializers.save_hdf5('{0}_{1:03d}.model'.format(args.output, save_count), q)
                    chainer.serializers.save_hdf5('{0}_{1:03d}.state'.format(args.output, save_count), optimizer)
                    save_iter = 10000
                    save_count += 1
            current_clock = time.clock()
            wait = next_clock - current_clock
            print('wait: ', wait)
            if wait > 0:
                next_clock += INTERVAL
                time.sleep(wait)
            elif wait > -INTERVAL / 2:
                next_clock += INTERVAL
            else:
                next_clock = current_clock + INTERVAL
    except KeyboardInterrupt:
        pass
