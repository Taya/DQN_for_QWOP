import logging
import os
import random
import time

import pyautogui
import pytesseract
from PIL import Image


class Controller:
    IMG_NAME_LIST = ["start", "restart", "meters", "game_screen"]
    STATE_TITLE = 0
    STATE_PLAY = 1
    STATE_RESULT = 2
    WIDTH = 600
    HEIGHT = 450
    ACTIONS = [[key, j, 1 - j] for key in ["q", "w", "o", "p"] for j in range(2)]

    def __init__(self, width=WIDTH, height=HEIGHT):
        print(self.ACTIONS)
        self.x = 0
        self.y = 0
        self.width = width
        self.height = height
        self.random_count = 0
        self.state = self.STATE_TITLE
        self.adjust_state_count = 0
        self.images = {}
        self.pre_meter = 0
        self.current_meter = 0
        self.pausing_play = False
        self.random_prev_pos = 0

    def set_meters(self, current_meter):
        self.pre_meter = self.current_meter
        self.current_meter = current_meter

    def set_position(self, x, y):
        self.x = x
        self.y = y

    def region(self):
        return self.x, self.y, self.width, self.height

    @staticmethod
    def find_image(screenshot, image, x=0, y=0, w=None, h=None, center=False):
        if w is None:
            right = screenshot.width
        else:
            right = x + w
        if h is None:
            bottom = screenshot.height
        else:
            bottom = y + h
        # cut a part of screenshot image
        cropped = screenshot.crop((x, y, right, bottom))
        position = pyautogui.locate(image, cropped)
        if position is not None:
            if center:
                return x + position[0] + position[2] / 2, y + position[1] + position[3] / 2
            else:
                return x + position[0], y + position[1]
        return None

    def move_to(self, x, y):
        pyautogui.moveTo(x + self.x, y + self.y)

    @staticmethod
    def image_path(filename):
        return os.path.join('./images', filename)

    def get_game_region(self):
        logging.debug('Finding game region...')
        region = pyautogui.locateOnScreen(self.image_path('start.png'))
        if region is None:
            raise Exception('Could not find game on screen. Is the game visible?')

        # calculate the region of the entire game
        topRightX = region[0] + region[2]  # left + width
        topRightY = region[1]  # top
        GAME_REGION = (topRightX - 640, topRightY, 640, 480)  # the game screen is always 640 x 480
        logging.debug('Game region found: %s' % (GAME_REGION,))

    def get_meter(self, image):
        # image = Image.open(self.image_path(filename))
        number = pytesseract.image_to_string(image)
        return number

    def load_images(self, dir_path):
        for name in self.IMG_NAME_LIST:
            self.images[name] = Image.open(os.path.join(dir_path, '{}.png'.format(name)))

    def detect_position(self):
        screenshot = pyautogui.screenshot()
        # position = pyautogui.locateOnScreen(self.images["game_screen"])
        #
        # if position is not None:
        #     x = position[0]
        #     y = position[1]
        #     self.set_position(x, y)
        #     return x, y
        # return None
        position = self.find_image(screenshot, self.images["start"])
        offset_x = 288
        offset_y = 252

        if position is not None:
            x, y = position
            x -= offset_x
            y -= offset_y
            self.set_position(x, y)
            return x, y
        return None

    def action_size(self):
        return len(self.ACTIONS)

    # game playing method (need to change)
    def play(self, action):
        key, up, down = self.ACTIONS[action]
        if up > 0:
            pyautogui.keyUp(key)
        elif down > 0:
            pyautogui.keyDown(key)
        else:
            raise Exception("Unexpected action" + str(self.ACTIONS[action]))

    def randomize_action(self, action, random_probability):
        prev_pos = self.random_prev_pos
        pos_size = self.action_size() // 2
        if random.random() * 15 < random_probability:
            self.random_count += random.randint(1, 29)
            prev_pos = random.randint(0, pos_size - 1)
        if self.random_count > 0:
            self.random_count -= 1
            pos = prev_pos
            button = random.randint(0, 1)
            if pos < 0:
                pos = 0
            elif pos >= pos_size - 1:
                pos = pos_size - 1
            action = pos * 2 + button
            prev_pos += random.randint(-5, 5)
        self.random_prev_pos = prev_pos
        return action

    def adjust_state(self, screen):
        # position = self.find_image(screen, self.images['start'], 270, 240, 60, 40)
        position = pyautogui.locateOnScreen(self.images["start"])
        if position is not None:
            self.state = self.STATE_TITLE
            return
        # position = self.find_image(screen, self.images['restart'], 10, 16, 60, 40)
        position = pyautogui.locateOnScreen(self.images["restart"])
        if position is not None:
            self.state = self.STATE_RESULT
            return

    def process(self, screen):
        self.adjust_state_count -= 1
        if self.adjust_state_count <= 0:
            self.adjust_state(screen)
            self.adjust_state_count = 100

        if self.state == self.STATE_TITLE:
            print("state = title")
            return self._process_title(screen)
        elif self.state == self.STATE_RESULT:
            print("state = result")
            return self._process_result(screen)
        else:
            print("state = play")
            return self._process_play(screen)

    def _process_title(self, screen):
        self.move_to(0, 0)
        time.sleep(0.1)
        # position = self.find_image(screen, self.images['start'], 270, 240, 125, 23, True)
        position = pyautogui.locateCenterOnScreen(self.images["start"])
        print(position)
        if position is not None:
            x, y = position
            pyautogui.moveTo(position)
            time.sleep(0.1)
            pyautogui.click(clicks=2)
            time.sleep(3)
            self.state = self.STATE_PLAY
        return None, False

    def _process_play(self, screen):
        position = self.find_image(screen, self.images['restart'], self.x, self.y, 100, 100, True)
        # position = pyautogui.locateCenterOnScreen(self.images["restart"])
        if position is not None:
            self.pausing_play = False
            self.state = self.STATE_RESULT
            return 0, True

        position = self.find_image(screen, self.images['meters'], 284, 187, 28, 20, True)
        # position = pyautogui.locateOnScreen(self.images["meters"])
        if position is not None:
            print("meters: " + position)
            if self.pausing_play:
                return None, False

            x, y = position
            meter_image = pyautogui.screenshot(region=(x - 50, y, 50, 70))
            print("get_meter: " + self.get_meter(meter_image))
            self.set_meters(self.get_meter(meter_image))
            # self.pausing_play = True
            return self.current_meter - self.pre_meter, True
        if self.pausing_play:
            self.pausing_play = False
        return 0, False

    def _process_result(self, screen):
        self.move_to(0, 0)
        time.sleep(0.1)
        # position = self.find_image(screen, self.images['restart'], 460, 406, 60, 40, True)
        position = pyautogui.locateCenterOnScreen(self.images["restart"])
        if position is not None:
            pyautogui.moveTo(position)
        else:
            print("can not position restart")

        time.sleep(0.1)
        pyautogui.press("space")
        time.sleep(3)
        self.state = self.STATE_PLAY
        return None, False
